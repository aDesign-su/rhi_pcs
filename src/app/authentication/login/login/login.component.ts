import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../auth.service';
import {Router} from '@angular/router'

type Engine = /*unresolved*/ any;
type Container = /*unresolved*/ any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public user: any;
  hide = true;
  username!: string;
  password!: string;
  formSubmitted = false;
  messageError = '';
  passwordError = '';

  constructor(public AuthService: AuthService, public  router:Router) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
  }

  errorMessage = ''
  login() {
    this.formSubmitted = true;
    this.messageError = '';
    this.passwordError = '';

    if (!this.username || !this.password) {
      return;
    }

    this.AuthService.slogin(this.username, this.password).subscribe(
      (response) => {
        console.log(response);
        // redirect to home page or some other page
      },
      (error) => {
        console.log(error);
          this.messageError = 'Неверное имя пользователя или пароль!';
      });
  }
  
 
  refreshToken() {
    this.AuthService.refreshToken();
  }
 
  logout() {
    this.AuthService.logout();
  }
}
