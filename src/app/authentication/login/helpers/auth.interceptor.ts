import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        () => { },
        (error) => {
          setTimeout(() => {
          if (error.status === 401) {
            // Обработка статуса 401 Unauthorized
            localStorage.removeItem('access');
            this.router.navigate(['/login']);
          }
        }, 2000);
        }
      )
    );
  }
}
