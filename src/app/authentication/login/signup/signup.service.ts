import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';
import { signup } from './signup';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  baseUrl = 'http://darkdes-django-z38bs.tw1.ru/api/v1';

constructor(private http: HttpClient) { }

  Register(user: signup) {
    return this.http.post(`${this.baseUrl}/registration/`, user).pipe(
      catchError((error) => {
        if (error.status === 400) {
          return throwError('Пользователь с таким именем пользователя уже существует.');
        } else {
          return throwError('Не удалось зарегистрироваться. Попробуйте еще раз.');
        }
      })
    );
  }
}
