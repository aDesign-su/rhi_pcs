import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { signup } from './signup';
import { SignupService } from './signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  hide = true;
  regs: signup[] = [];
  reg: signup = {'last_name':'','first_name':'','username':'','email':'','password':''}
  public form!: FormGroup;

  accessDenied!: boolean
  errorMessage: string | null = null;
  error = '';
  success = '';

  resetAlerts() {
    this.error = '';
    this.success = '';
  }

  constructor(private fb: FormBuilder,private SignupService: SignupService) { }

  ngOnInit() {
    this.form = this.fb.group({
      first_name: ['', [Validators.required,Validators.pattern(/^[а-яёА-ЯЁ]+$/u)]],
      last_name: ['', [Validators.required,Validators.pattern(/^[а-яёА-ЯЁ]+$/u)]],
      username: ['', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]+$/)]],
      email: ['',[Validators.email]],
      password: ['', [Validators.required,Validators.minLength(8)]]
    });
  }

  addUser() {
    this.resetAlerts();
    
    if (this.form.valid) {
      const user: signup = this.form.value;
      this.SignupService.Register(user).subscribe(
        response => {
          // handle successful registration
          this.accessDenied = true;
          this.errorMessage = null;
        },
        error => {
          this.errorMessage = error;
        }
      );
    }
  }
}
