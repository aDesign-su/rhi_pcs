import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private msgData: {message: string, read: boolean}[] = [
    {message: 'Вы назначены исполнителем по пакету работ №FNJ003', read: true},
    {message: 'Документ №CWP02-RFQ4 утверждён', read: true},
    {message: 'На согласование направлен документ №CWP02-RFQ4', read: true},
  ];

  addMessage(newMessage: string) {
    this.msgData.unshift({message: newMessage, read: false});
  }

  getMessages() {
    return this.msgData;
  }
}