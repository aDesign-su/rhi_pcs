import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { CurrencyExchange, DiscountRate, ResultItem, Wacc, Parameters, ParameterValue } from './parameter';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as jmespath from 'jmespath';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-macro',
  templateUrl: './macro.component.html',
  styleUrls: ['./macro.component.css']
})
export class MacroComponent implements OnInit {
  constructor(public theme: ThemeService,private macro:ApicontentService,private formBuilder: FormBuilder) {
    this.addDiscountRateForm = this.formBuilder.group({
      parameter: '',
      source: '',
      value: '',
    });
    this.addParametersForm = this.formBuilder.group({
      type_parameter: '',
      name: '',
      base: '',
    });
    this.addParameterValueForm = this.formBuilder.group({
      year: 0,
      parameter: '',
      value: '',
    });
   }

  ngOnInit() {
    this.Parameter(); this.Wacc(); this.SelectData()
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  // displayedColumns!: string[]
  dataSource1 = new MatTableDataSource<CurrencyExchange>([]);
  dataSource2 = new MatTableDataSource<Wacc>([]);

  displayedColumns1: string[] = ['param_1', 'param_2', 'param_3','2024','2025','2026','2027','2028','2029','2030','2031','2032'];
  displayedColumns2: string[] = ['param_1', 'param_2', 'param_3'];

  @ViewChild('paginator1') paginator1!: MatPaginator;
  @ViewChild('paginator2') paginator2!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource1.paginator = this.paginator1;
    this.dataSource2.paginator = this.paginator2;
  }

  block1 = false
  block2 = false
  block3 = false

  addDiscountRateForm!: FormGroup;
  addParametersForm!: FormGroup;
  addParameterValueForm!: FormGroup;

  getParameter: ResultItem[] = []
  getWacc: Wacc[] = []

  getType: any[] = []
  selectedType!: string;
  getName: any[] = []
  selectedName!: string;
  closedAdd() {
    this.block1 = false
    this.block2 = false
    this.block3 = false
  }
  toggleDiscountRate() {
    this.block1 = !this.block1
    this.block2 = false
    this.block3 = false
  }
  toggleParameters() {
    this.block2 = !this.block2
    this.block1 = false
    this.block3 = false
  }
  toggleParameterValue() {
    this.block3 = !this.block3
    this.block1 = false
    this.block2 = false
  }
  Parameter() {
    this.macro.getParameter().subscribe(
      (response: CurrencyExchange[]) => {
        const arr: ResultItem[] = [];
        response.forEach(item => {
          const year = item.year.toString();
          const value = parseFloat(item.value).toFixed(2);
          const parameter1 = item.parameter.type_parameter;
          const parameter2 = item.parameter.name;
          const parameter3 = item.parameter.base;
          const id = item.parameter.id;

          let obj = arr.find(element => element['id'] === id);
          if (!obj) {
            obj = {
              'id': id,
              'param_1': parameter1,
              'param_2': parameter2,
              'param_3': parameter3
            };
            arr.push(obj);
          }

          obj[year] = value;
        });

        this.dataSource1 = new MatTableDataSource<any>(arr);
        this.dataSource1.paginator = this.paginator1;

      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  Wacc() {
    this.macro.getWacc().subscribe(
      (response: Wacc[]) => {
        this.getWacc = response
        this.dataSource2 = new MatTableDataSource<Wacc>(this.getWacc);
        this.dataSource2.paginator = this.paginator2;
      },
    );
  }
  SelectData() {
    this.macro.getSelect().subscribe(
      (response: any[]) => {

        const type = "[*].types[]";
        this.getType = jmespath.search(response, type);

        const name = "[*].names[]";
        this.getName = jmespath.search(response, name);
         console.log( this.getType)
         console.log( this.getName)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  DiscountRate() {
    const addDiscountRate = this.addDiscountRateForm.value;
    this.macro.addDiscountRate(addDiscountRate).subscribe(
      (response: DiscountRate[]) => {
        console.log('Data added successfully:', response);
        this.addDiscountRateForm.reset();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  Parameters() {
    const addParameters = this.addParametersForm.value;
    this.macro.addParameters(addParameters).subscribe(
      (response: Parameters[]) => {
        console.log('Data added successfully:', response);
        this.addParametersForm.reset();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  ParameterValue() {
    const addParameterValue = this.addParameterValueForm.value;
    this.macro.addParameterValue(addParameterValue).subscribe(
      (response: ParameterValue[]) => {
        console.log('Data added successfully:', response);
        this.addParameterValueForm.reset();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
}
