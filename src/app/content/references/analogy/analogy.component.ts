import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Equipments, WorkPackages, analogBudgets } from './analogy';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as jmespath from 'jmespath';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-analogy',
  templateUrl: './analogy.component.html',
  styleUrls: ['./analogy.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class AnalogyComponent implements OnInit {
  constructor(public theme: ThemeService,public analogService:ApicontentService,private formBuilder: FormBuilder,private _adapter: DateAdapter<any>,@Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.analogForm = this.formBuilder.group({
      title: '',
      constr_smr: 0,
      date_start: '',
      date_end: '',
      eng_pir: 0,
      tech_description: '',
      cost: 0,
      type: '',
      region:''
    });
    this.workPackage = this.formBuilder.group({
      "title": "",
      "type": "",
      "contractor": "",
      "cost_total": 0,
      "cost_pp": 0,
      "mh": 0,
      "comments": "",
      "project": 0,
      "region": 0
    });
    this.equipmenForm = this.formBuilder.group({
      "title": "",
      "type": "",
      "tech_description": "",
      "cost": 0,
      "description": "",
      "project": 0,
      "manufacturer": 0
    });
   }

  ngOnInit() { this.getDataSource(),this.Сomparison() }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  performJmesPathQuery(data: any, query: string): any {
    return jmespath.search(data, query);
  }
  displayedColumns: string[] = [
    'title',
    'constr_smr',
    'date_start',
    'date_end',
    'eng_pir',
    'tech_description',
    'cost',
    'type',
    'region',
    'action'
  ];
  dataSource = new MatTableDataSource<analogBudgets>([]);
  anlData: analogBudgets[] = []
  equData: analogBudgets[] = []
  wrkData: WorkPackages[] = []
  analog: analogBudgets[] = []
  getAnalogBudget: analogBudgets[] = []
  getEquipment: any = []
  getWorkPackage: any = []
  getAnalogBudgets: any = []
  getComparison: any = []

  getSelect: any[] = []
  selectedRegion!: string;
  getPackage: any[] = []
  PackageWork!: string;

  block = false;
  block2 = false;
  showBlock1 = false;
  showBlock2 = false;
  showBlock3 = false;
  analogForm!: FormGroup;
  workPackage!: FormGroup;
  equipmenForm!: FormGroup;

  createEqu() {
    this.showBlock3 = !this.showBlock3;
    this.showBlock2 = false;
  }
  createPac() {
    this.showBlock2 = !this.showBlock2;
    this.showBlock3 = false;
  }
  create() {
    this.showBlock1 = !this.showBlock1;
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  // compare() {
  //   this.block2 = !this.block2;
  // }
  createBudget() {
    this.showBlock1 = !this.showBlock1;
  }
  createWork() {
    this.showBlock2 = !this.showBlock2;
  }
  createAnalog() {
    this.showBlock3 = !this.showBlock3;
  }
  closedAdd() {
    this.showBlock1 = false
    this.showBlock2 = false
    this.showBlock3 = false
    this.block = false
  }
  Сomparison() {
    this.analogService.getСomparison().subscribe(
      (response: any[]) => {
        this.getComparison = response
        const anl = "[*].analog_budget|[]";
        this.anlData = jmespath.search(response, anl);
        console.log('anl',this.anlData);

        const equ = "[*].Equipment|[]";
        this.equData = jmespath.search(response, equ);
        console.log('equ',this.equData);

        const wrk = "[*].Work_packages|[]";
        this.wrkData = jmespath.search(response, wrk);
        console.log(this.wrkData);
      },
      (error: any) => {
        console.error('Failed to add data:', error);

      }
    );
  }
  showDetails(element: analogBudgets) {
    this.block = !this.block;
    if (element.id) {
      this.getDataAnalogId(element.id);
    }
  }
  getDataAnalogId(AnalogId: number): void {
    this.analogService.getAnalogId(AnalogId).subscribe(
      (data: analogBudgets[]) => {
        // this.analogData = data;
        const equ = "[*].equipment|[]";
        this.getEquipment = jmespath.search(data, equ);
        console.log(this.getEquipment);

        const wrk = "[*].work_package|[]";
        this.getWorkPackage = jmespath.search(data, wrk);
        console.log(this.getWorkPackage);

        const anl = "[*].analog_budget|[]";
        this.getAnalogBudgets = jmespath.search(data, anl);
        console.log(this.getAnalogBudgets);

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDataSource(): void {
    this.analogService.getAnalogBudget().subscribe(
      (data: analogBudgets[]) => {
        //main select
        const query = "[*].regions|[*]|[]";
        this.getSelect = jmespath.search(data, query);

        //select work_package
        const response = "[*].{id: id, title: title}";
        const getResponse = jmespath.search(data, response);
        getResponse.pop();
        this.getPackage = getResponse.slice(0, getResponse.length - 0);

        data.pop();
        this.getAnalogBudget = data.slice(0, data.length - 0);

        this.dataSource.data = this.getAnalogBudget;
        this.dataSource.paginator?.firstPage();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  AnalogBudget() {
    const analogBudget = this.analogForm.value;
    this.analogService.addAnalogBudget(analogBudget).subscribe(
      (response: analogBudgets[]) => {
        console.log('Data added successfully:', response);
        this.analogForm.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addWorkPackage() {
    const WorkPackage = this.workPackage.value;
    this.analogService.addWorkPackage(WorkPackage).subscribe(
      (response: WorkPackages[]) => {
        console.log('Data added successfully:', response);
        this.workPackage.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addEquipments() {
    const Equipment = this.equipmenForm.value;
    this.analogService.addEquipment(Equipment).subscribe(
      (response: Equipments[]) => {
        console.log('Data added successfully:', response);
        this.equipmenForm.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
}
