export interface TableData {
  no: number;
  progress: number;
  subSystem: string;
  drawingNo: string;
  managerRH: string;
  specialistRHIProcurement: string;
  specialistRHIControl: string|null;
  isButton: boolean,
  isSelected: boolean,
  budget: string,
  weightTotal: number;
  concreteAORPI: number;
  weightTotal2: number;
  reinforcementAORPI: number;
  weightTotal3: number;
  anchorBoltsAORPI: number;
  weightTotal4: number;
  gravelAORPI: number;
  weightTotal5: number;
  sandAORPI: number;
  electronicArchive: string;
}

export const dataSource: TableData[] = [
  {
    no: 1,
    progress: 92,
    subSystem: '4.1.1.20.703-KJ104',
    drawingNo: '0055-4.1.1.20.703-KJ104.DT-0004',
    managerRH: 'Сергеев',
    specialistRHIProcurement: 'Петров',
    specialistRHIControl: 'Иванов',
    isSelected: false,
    isButton: true,
    budget: '',
    weightTotal: 30,
    concreteAORPI: 50,
    weightTotal2: 80,
    reinforcementAORPI: 30,
    weightTotal3: 70,
    anchorBoltsAORPI: 20,
    weightTotal4: 60,
    gravelAORPI: 10,
    weightTotal5: 40,
    sandAORPI: 20,
    electronicArchive: 'Yes'
  },
  {
    no: 2,
    progress: 30,
    subSystem: '	4.1.1.20.703-KJ104',
    drawingNo: '0055-4.1.1.20.703-KJ104.DT-0004',
    managerRH: 'Сергеев',
    specialistRHIProcurement: 'Петров',
    isButton: true,
    isSelected: false,
    budget: '',
    specialistRHIControl: null,
    weightTotal: 20,
    concreteAORPI: 60,
    weightTotal2: 90,
    reinforcementAORPI: 40,
    weightTotal3: 80,
    anchorBoltsAORPI: 30,
    weightTotal4: 70,
    gravelAORPI: 20,
    weightTotal5: 50,
    sandAORPI: 30,
    electronicArchive: 'No'
  },
  {
    no: 3,
    progress: 61,
    subSystem: '	4.1.1.20.703-KJ104',
    drawingNo:'0055-4.1.1.20.703-KJ104.DT-0004',
    managerRH: 'Сергеев',
    specialistRHIProcurement: 'Петров',
    specialistRHIControl: 'Иванов',
    isButton: true,
    isSelected: false,
    budget: '',
    weightTotal: 80,
    concreteAORPI: 40,
    weightTotal2: 60,
    reinforcementAORPI: 20,
    weightTotal3: 50,
    anchorBoltsAORPI: 10,
    weightTotal4: 40,
    gravelAORPI: 50,
    weightTotal5: 30,
    sandAORPI: 10,
    electronicArchive: 'Yes'
  },
  {
    no: 4,
    progress: 62,
    subSystem: '4.1.1.20.703-KJ104',
    drawingNo: '0055-4.1.1.20.703-KJ104.DT-0004',
    managerRH: 'Сергеев',
    specialistRHIProcurement: 'Петров',
    specialistRHIControl: 'Иванов',
    isButton: true,
    isSelected: false,
    budget: '',
    weightTotal: 10,
    concreteAORPI: 80,
    weightTotal2: 10,
    reinforcementAORPI: 50,
    weightTotal3: 90,
    anchorBoltsAORPI: 40,
    weightTotal4: 80,
    gravelAORPI: 30,
    weightTotal5: 70,
    sandAORPI: 40,
    electronicArchive: 'No'
  }
];