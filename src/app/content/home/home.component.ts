import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { TableData, dataSource } from './data';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import { MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { ModaDialogComponent } from './moda-dialog/moda-dialog.component';
import { DataService } from '../../header/msg.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit {

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');

  }
  
  stickyColumns = ['no', 'progress', 'subSystem'];
  stickyHeaders = ['header-1', 'header-2'];
  stickyFooters = ['footer-1', 'footer-2'];

  displayedColumns = ['no', 'progress', 'subSystem', 'drawingNo', 'managerRH', 'specialistRHIProcurement', 'specialistRHIControl', 'weightTotal', 'concreteAORPI', 'weightTotal2', 'reinforcementAORPI', 'weightTotal3', 'anchorBoltsAORPI', 'weightTotal4', 'gravelAORPI', 'weightTotal5', 'sandAORPI', 'electronicArchive',];
  data = dataSource

  /** Whether the button toggle group contains the id as an active value. */
  isSticky(buttonToggleGroup: MatButtonToggleGroup, id: string) {
    return (buttonToggleGroup.value || []).indexOf(id) !== -1;
  }

  constructor(public theme: ThemeService, public dialog: MatDialog,private dataService: DataService) { }

  // openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
  //   this.dialog.open(ModaDialogComponent);
  // }
  ngOnInit() {
  }

  onRadioChange(element: TableData) {
    // Обновляем свойство isSelected для элемента
    element.isSelected = true;

    // Перебираем все элементы dataSource, чтобы сбросить isSelected для всех остальных элементов
    this.data.forEach(item => {
      if (item !== element) {
        item.isSelected = false;
      }
    });
  }
  openDialog(element: TableData): void {
    const dialogRef = this.dialog.open(ModaDialogComponent, {
      width: '30%',
      data: { names: ['Name1', 'Name2', 'Name3'], chosenName: '' }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.dataService.addMessage("Вы назначили исполнителем по пакету работ 0055-4.1.1.20.703-KJ104.DT-0004 - " + result);
      element.specialistRHIControl = result; // Присваиваем полученное имя нужному полю в вашем dataSource

    });
  }

}
