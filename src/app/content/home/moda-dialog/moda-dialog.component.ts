import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  names: string[];
  chosenName: string;
}



@Component({
  selector: 'app-moda-dialog',
  templateUrl: './moda-dialog.component.html',
  styleUrls: ['./moda-dialog.component.css'],
})
export class ModaDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ModaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close(this.labelPosition); 
    console.log(this.labelPosition)
  }
  getBadge() {
    if (this.labelPosition === '4') {
      this.budget = 'Ваш бюджет для Алексеева'; // Установите значение budget для опции 'Алексеев'
    } else {
      this.budget = ''; // Очистите значение budget для других опций
    }
  }
  budget!: string
  labelPosition!: string;
}

