export interface cardObj {
    id?: number,
    name: string,
    sdr_id: string,
    assoi_id: number,
    total_cost: number,
    pir_cost: number,
    smr_cost: number,
    equipment_cost: number,
    other_cost: number,
    direction_id: string
    editing?: boolean;
}
export interface Direction  {
    id: number;
    name: string;
}
