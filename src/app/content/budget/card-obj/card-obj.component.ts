import { Component, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, style, animate, transition } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cardObj, Direction } from './card-obj';
import { ApicontentService } from 'src/app/api.content.service';

@Component({
  selector: 'app-card-obj',
  templateUrl: './card-obj.component.html',
  styleUrls: ['./card-obj.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 1, transform: 'translateY(-20px)' }),
        animate('0.2s ease-in-out', style({ opacity: 1, transform: 'translateY(0)' }))
      ]),
      transition(':leave', [
        animate('0.4s ease-in-out', style({ opacity: 1, transform: 'translateY(-350px)' }))
      ])
    ])
  ]
})

export class CardObjComponent implements OnInit {
  constructor(public theme: ThemeService,private fb: FormBuilder,public cardObjService:ApicontentService) { 
    this.directions = [];
    
   }
   
  public form!: FormGroup;
  cards: cardObj[] = [];
  card: cardObj = {
    'name':'',
    'sdr_id':'',
    'assoi_id':0,
    'total_cost':0,
    'direction_id':'',
    'pir_cost':1,
    'smr_cost':1,
    'equipment_cost':1,
    'other_cost':1
  }
  displayedColumns: string[] = [
    'id',
    'sdr_id',
    'assoi_id',
    'name',
    'total_cost',
    'pir_cost',
    'smr_cost',
    'equipment_cost',
    'direction_id',
    'other_cost',
    'action'
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  directions: { id: number; name: string }[];
  options: string[] = [];

  errorMessage: string | null = null;
  accessDenied!: boolean
  showBlock1 = false;
  error = '';
  success = '';
  dataSource: MatTableDataSource<cardObj> = new MatTableDataSource<cardObj>([]);
  
  selectedFile: File | null = null;
  message: string = ''
  numberPattern = /^[0-9]*$/;
  floatNumberPattern = /^\d+(\.\d+)?$/;
  resetAlerts() {
    this.error = '';
    this.success = '';
  }
  resetForm() {
    this.form.reset();
    this.isFileUploaded = false; 
  }
  closedAdd() {
    this.showBlock1 = false
  }
  create() {
    this.showBlock1 = !this.showBlock1;
  }
  
  // Загрузка файла
  chooseFile() {
    const fileInput = document.querySelector('#csvInput') as HTMLInputElement;
    fileInput.click();
  }
  
  csvInputChange(fileInputEvent: any) {
    this.selectedFile = fileInputEvent.target.files[0];
  }
  isFileUploaded: boolean = false;
  uploadFile() {
    if (this.selectedFile) {
      this.cardObjService.uploadFile(this.selectedFile).subscribe(
        (response) => {
          console.log('Файл успешно загружен.', response);
          this.message = 'Файл успешно загружен.'
          this.isFileUploaded = true;

        // Сбросить значения формы
          this.form.reset();
          
        // Обновить данные
        this.cardObjService.getData().subscribe(
          (response: any) => {
            response.pop();
            const data = response.map((item: any, index: number) => {
              return { ...item, orderNumber: index + 1 };
            });
            this.dataSource.data = data;
          },
          (error: any) => {
            console.error('Ошибка при получении данных:', error);
          }
        );
          setTimeout(() => {
            this.selectedFile = null;
            this.message = '';
            this.isFileUploaded = false; 
          }, 2500);

          this.paginator.pageIndex = 0; // Установить индекс страницы на первую страницу
          this.paginator.pageSize = this.paginator.pageSizeOptions[0]; // Установить размер страницы на первый вариант из pageSizeOptions
          this.paginator._changePageSize(this.paginator.pageSize); 
          
        },
        (error) => {
          console.error('Ошибка при загрузке файла.', error);
          this.message = 'Ошибка при загрузке файла.'

          setTimeout(() => {
            this.selectedFile = null;
            this.message = '';
            this.isFileUploaded = false; 
          }, 2500);
        }
      );
    } else {
      console.log('Файл не выбран.');
      this.message = 'Файл не выбран.'

      setTimeout(() => {
        this.selectedFile = null;
        this.message = '';
        this.isFileUploaded = false; 
      }, 2500);
    }
  }

  ngOnInit() {
    // Перезагружаем страницу, чтобы не потерять токен (костыль)
    if (!localStorage.getItem('foo')) { 
      localStorage.setItem('foo', 'no reload') 
      location.reload() 
    } else {
      localStorage.removeItem('foo') 
    }
    this.form = this.fb.group({
      name: ['', [Validators.maxLength(100)]],
      sdr_id: ['', [Validators.maxLength(15)]],
      assoi_id: [,[Validators.pattern(this.numberPattern)]],
      total_cost: [, [Validators.pattern(this.floatNumberPattern)]],
      pir_cost: [0],
      smr_cost: [0],
      equipment_cost: [0],
      other_cost: [0],
      direction_id: [,],
    });

    this.cardObjService.getData().subscribe(
      (response: any) => {
        response.pop();
        const data = response.map((item: any, index: number) => {
          return { ...item, orderNumber: index + 1 };
        });
        this.dataSource.data = data; // Обновить данные в dataSource
        const jsonObject = response[response.length - 1];
        this.directions = jsonObject.directions;
      }
    );
    this.getDirections();
  }
  
  getDirections(): void {
    this.cardObjService.getData().subscribe((response: any) => {
      const jsonObject = response[response.length - 1];
      this.directions = jsonObject.directions;
    });
  }

  addCard() {
    if (this.form.invalid) {
      return;
    }
    this.resetAlerts();
    if (this.form.valid) {
      const card: cardObj = this.form.value;
      this.cardObjService.cradObj(card).subscribe(
        (response:any) => {
          this.directions = response.directions;
          // handle successful registration
          this.accessDenied = true;
          this.errorMessage = null;

          // сброс формы и данных в матрице 
          this.resetForm();
          this.cardObjService.getData().subscribe((response: any) => {
            this.dataSource = response; // Присвоить обновленные данные переменной dataSource
            const jsonObject = response[response.length - 1];
            this.directions = jsonObject.directions; // Обновить массив directions
          });
          // отобразить в таблице при добавлении
          this.getCard();
        },
        error => {
          this.errorMessage = error;
        }
      )
    }
  }
  getCard(): void {
    this.cardObjService.getData().subscribe(
      (response: any) => {
        response.pop();
        this.dataSource = response.map((item: any, index: number) => {
          return { ...item, orderNumber: index + 1 };
        });
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  remove(id: number) {
    this.cardObjService.deleteData(id).subscribe(
      () => {
        console.log('Data deleted successfully');
        // Обновить данные в матрице после успешного удаления
        this.cardObjService.getData().subscribe((response: any) => {
          response.pop();
          this.dataSource = response.map((item: any, index: number) => {
            return { ...item, orderNumber: index + 1 };
          }); // Присвоить обновленные данные переменной dataSource
        });
      },
      error => {
        console.error('Failed to delete data:', error);
        // Обработайте ошибку при удалении данных
      }
    );
  }
  edit(element: any) {
    element.editing = true;
  }
  save(element: any): void {
    element.editing = false;

    const id = element.id; // Получите идентификатор объекта из объекта `element`
    const updatedData = { 
      name: element.name, 
      sdr_id: element.sdr_id, 
      assoi_id: element.assoi_id, 
      total_cost: element.total_cost, 
      pir_cost: element.pir_cost,
      smr_cost: element.smr_cost,
      equipment_cost: element.equipment_cost,
      other_cost: element.other_cost,
      direction_id: element.direction_id
    }; // Используйте актуальные данные из объекта `element`
  
    this.cardObjService.updateData(id, updatedData).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        element.editing = false; // Установите свойство `editing` в значение `false` для объекта `element` после успешного обновления данных
        this.getCard();
      },
      (error: any) => {
        console.error('Failed to update data:', error);
        // Обработайте ошибку при обновлении данных
      }
    );
  }

}

