import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Estimated, analog, workPackage } from './estimated';
import * as jmespath from 'jmespath';
import { SelectionModel } from '@angular/cdk/collections';
import { analogBudgets, WorkPackages, Equipments } from '../../references/analogy/analogy';
import { trigger, style, animate, transition } from '@angular/animations';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';

@Component({
  selector: 'app-estimated',
  templateUrl: './estimated.component.html',
  styleUrls: ['./estimated.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 1, transform: 'translateY(-120px)' }),
        animate('0.2s ease-in-out', style({ opacity: 1, transform: 'translateY(0)' }))
      ]),
      transition(':leave', [
        animate('0.4s ease-in-out', style({ opacity: 1, transform: 'translateY(-1000px)' }))
      ])
    ])
  ],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },

    // `MomentDateAdapter` and `MAT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your application's root module. We provide it at the component level
    // here, due to limitations of our example generation script.

    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class EstimatedComponent implements OnInit {
  constructor(public theme: ThemeService, private estimService: ApicontentService, private formBuilder: FormBuilder,private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.analogForm = this.formBuilder.group({
      title: "",
      constr_smr: "",
      date_start: "",
      date_end: "",
      eng_pir: "",
      tech_description: "",
      cost: "",
      type: "",
      region: "",
      estimated_budget: ""  
    });
    this.workPackage = this.formBuilder.group({
      title: "",
      type: "",
      contractor: "",
      cost_total: null,
      cost_pp: null,
      mh: null,
      comments: "",
      project: null,
      region: null,
      estimated_budget: null
    });
    this.equipmenForm = this.formBuilder.group({
      title: "",
      type: "",
      tech_description: "",
      cost: null,
      description: "",
      project: null,
      // manufacturer: null,
      estimated_budget: null
    });
    this.estimatedForm = this.formBuilder.group({
      id_assoi: "",
      id_SAP: "",
      id_srd: "",
      title: "",
      version: "",
      comments: "",
      macro: 0,
      cost: 0,
      currency: null
    });
  }
  analogForm!: FormGroup;
  workPackage!: FormGroup;
  equipmenForm!: FormGroup;
  estimatedForm!: FormGroup;

  ngOnInit() {
    this.Estimated()
    this.getDataSource()
    this.getEstimated()
    this.DataWorkPackage() 
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  selection = new SelectionModel<Estimated>(true, []);
  displayedColumns: string[] = ['select', 'id_assoi', 'id_SAP', 'id_srd', 'title', 'version', 'comments', 'macro', 'cost', 'currency', 'action'];
  dataSource = new MatTableDataSource<Estimated>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  getanalog: any = []
  getComparison: any = []
  getEquipment: any = []
  getWorkPackage: any = []
  getAnalogBudgets: any = []
  anlData: analogBudgets[] = []
  equData: analogBudgets[] = []
  wrkData: WorkPackages[] = []
  getAnalogBudget: analogBudgets[] = []

  block: boolean = false;
  showBlock1: boolean = false;
  showBlock2: boolean = false;
  showBlock3: boolean = false;
  showBlock4: boolean = false;
  showBlock5: boolean = false;

  selectedValue!: string;
  inputValue!: string;
  options: string[] = ['Option 1', 'Option 2', 'Option 3'];

  getEstimateds: any[] = []
  selectedEstimated!: string;
  getName: any[] = []
  selectedName!: string;
  getSelect: any[] = []
  selectedRegion!: string;
  getPackage: any[] = []
  PackageWork!: string;

  fields1: any[] = []
  fields2: any[] = []
  selectedField!: any;

  selectedType: any;  selectedTitle: any;  selectedContractor: any;  selectedCostTotal: any;  selectedCostPp: any;  selectedmh: any;  selectedComments: any;  selectedProject: any;  selectedRegions: any;
  selectedTitles: any; selectedTypes: any; selectedTech_description: any; selectedDescription: any; selectedProjects: any; selectedManufacturer: any; selectedCost: any;
  onSelectionChange1() {
    const selectedOption = this.fields1.find((option) => option.title === this.selectedTitle);

    if (selectedOption) {
      this.selectedTitle = selectedOption.title;
      this.selectedType = selectedOption.type;
      this.selectedContractor = selectedOption.contractor;
      this.selectedCostTotal = selectedOption.cost_total;
      this.selectedCostPp = selectedOption.cost_pp;
      this.selectedmh = selectedOption.mh;
      this.selectedComments = selectedOption.comments;
      this.selectedProject = selectedOption.project;
      this.selectedRegions = selectedOption.region;
    } else {
      // Обработайте ситуацию, если selectedOption не найден
      // Например, очистите значения полей
      this.selectedContractor = '';
      this.selectedCostTotal = null;
      this.selectedCostPp = null;
    }
  }
  onSelectionChange2() {
    const selectedOption = this.fields2.find((option) => option.title === this.selectedTitles);

    if (selectedOption) {
      this.selectedTitles = selectedOption.title;
      this.selectedTypes = selectedOption.type;
      this.selectedTech_description = selectedOption.tech_description;
      this.selectedCost = selectedOption.cost;
      this.selectedDescription = selectedOption.description;
      this.selectedProjects = selectedOption.project;
      // this.selectedManufacturer = selectedOption.manufacturer;
    } else {
      // Обработайте ситуацию, если selectedOption не найден
      // Например, очистите значения полей
      this.selectedContractor = '';
      this.selectedCostTotal = null;
      this.selectedCostPp = null;
    }
  }
  clickedRowsArray: Estimated[] = [];
  compared() {
    this.showBlock4 = !this.showBlock4;
  }
  createEqu() {
    this.showBlock3 = !this.showBlock3;
    this.showBlock2 = false;
  }
  createPac() {
    this.showBlock2 = !this.showBlock2;
    this.showBlock3 = false;
  }
  createObj() {
    this.showBlock5 = !this.showBlock5;
  }
  create() {
    this.showBlock1 = !this.showBlock1;
  }
  closedAdd() {
    this.showBlock1 = false
    this.showBlock2 = false
    this.showBlock3 = false
    this.showBlock4 = false
    this.showBlock5 = false
    this.block = false
  }

  closed(){
    this.showBlock2 = false
    this.showBlock3 = false
    this.showBlock5 = false
  }


  Estimated(): void {
    this.estimService.getEstimated().subscribe(
      (response: any) => {
        this.dataSource.data = response

        const query = "[*].{id: id, title: title}";
        this.getName = jmespath.search(response, query);
      },
      (error: any) => {
        console.error('Error:', error);
      }
    );
  }
  isAllSelected() {

    const numSelected = this.selection.selected?.length;
    const numRows = this.dataSource.data?.length;
    return numSelected === numRows;
  }


  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.clickedRows.clear();
      return;
    }

    this.dataSource.data.forEach(row => {
      this.selection.select(row);
      this.clickedRows.add(row);
    });
  }

  clickedRows = new Set<Estimated>();

  isRowClicked(row: Estimated): boolean {
    return this.clickedRows.has(row);
  }
  toggleRow(row: Estimated) {
    console.log('Выполнил ')
    if (this.isRowClicked(row)) {
      this.clickedRows.delete(row);
    } else {
      this.clickedRows.add(row);
    }

    this.clickedRowsArray = Array.from(this.clickedRows);
  }
  updateArray() {
    this.clickedRowsArray = Array.from(this.clickedRows);
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Estimated): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  DataWorkPackage() {
    this.estimService.getworkPackage().subscribe(
      (data: workPackage[]) => {
        // const query = "[*].{title: title}";
        // this.fields = jmespath.search(data, query);
        this.fields1 = data;
        console.log(data)
      }
    );
  }
  
  getDataSource(): void {
    this.estimService.getAnalogBudget().subscribe(
      (data: analogBudgets[]) => {
        //main select
        const query = "[*].regions|[*]|[]";
        this.getSelect = jmespath.search(data, query);

        //select work_package
        const response = "[*].{id: id, title: title}";
        const getResponse = jmespath.search(data, response);
        getResponse.pop();
        this.getPackage = getResponse.slice(0, getResponse.length - 0);

        data.pop();
        this.getAnalogBudget = data.slice(0, data.length - 0);

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getEstimated(): void {
    this.estimService.getEquipment().subscribe(
      (data: any[]) => {
        this.fields2 = data;
        console.log(this.fields2);
      }
    );
  }  
  getEstimatedId(EstimatedId: number): void {
    this.estimService.getEstimatedId(EstimatedId).subscribe(
      (data: Estimated[]) => {
        // this.analogData = data;
        const equ = "[*].equipment|[]";
        this.getEquipment = jmespath.search(data, equ);
        console.log(this.getEquipment);

        const wrk = "[*].work_package|[]";
        this.getWorkPackage = jmespath.search(data, wrk);
        console.log(this.getWorkPackage);

        const anl = "[*].analog_budget|[]";
        this.getAnalogBudgets = jmespath.search(data, anl);
        console.log(this.getAnalogBudgets);

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  } 
  addEstimated() {
    const estimated = this.estimatedForm.value;
    this.estimService.addEstimated(estimated).subscribe(
      (response: Estimated[]) => {
        console.log('Data added successfully:', response);
        this.estimatedForm.reset();
        this.Estimated();
        this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );

  }
  addWorkPackage() {
    const analogForms = this.analogForm.value

    // Format the date as YYYY-MM-DD
    const startdDate = moment(analogForms.date_start).format('YYYY-MM-DD');
    const endDate = moment(analogForms.date_end).format('YYYY-MM-DD');
    analogForms.date_start = startdDate;
    analogForms.date_end = endDate;
    this.closed()
    this.estimService.addBudget(analogForms).subscribe(
      (response: analog[]) => {
        this.analogForm.reset();
        
        // Handle success case
      },
    );
  }
    //Получение Аналог Бюджета
  getAnalogList(analogId:any): void {
    this.estimService.getwAnalog(analogId).subscribe(
      (response: analog[]) => {
        this.getanalog = response
        console.log('ok:', this.getanalog);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  addEquipments() {
    const Equipment = this.equipmenForm.value;
    this.estimService.addEquipment(Equipment).subscribe(
      (response: Equipments[]) => {
        console.log('Data added successfully:', response);
        this.equipmenForm.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  showDetails(element: Estimated) {
    this.block = true
    if (element.id) {
      this.getEstimatedId(element.id);
      // this.getAnalogList(element.id)
    }
  }
}
