import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { cardObj } from './content/budget/card-obj/card-obj';
import { Equipments, WorkPackages, analogBudgets } from './content/references/analogy/analogy';
import { acts, ContractsCreates, factDetails, invoices, items, listContracts, payments, paymentsFact, prepay_invoices } from './content/contract/register-contracts/reg-contracts';
import {
  DirectionFilter,
  ObjectFilter,
  SystemFilter,
  WorkFilter,
  EstimateFilter,
  ProjectFilter,
  Wbs,
  ElementFilter, PackageElements, DeleteEntity, CreateElement, ChangeElement
} from './content/budget/wbs/wbs';
import { CurrencyExchange, Wacc, DiscountRate, Parameters, ParameterValue } from './content/references/macro/parameter';
import { Estimated, analog, workPackage } from './content/budget/estimated/estimated';


@Injectable({
  providedIn: 'root'
})
export class ApicontentService {
private httpOptions: any;
private httpFile: any;

constructor(private http: HttpClient) {
  this.httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('access'),
    })
  };
  this.httpFile = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('access'),
    })
  };
 }


baseUrl = 'http://darkdes-django-z38bs.tw1.ru';

// Карточка объекта => добавление / вывод / обновление / файл
  cradObj(card: cardObj) {
    return this.http.post(`${this.baseUrl}/objects/`, card, this.httpOptions)
  }
  getData() {
    return this.http.get<any>(`${this.baseUrl}/objects/`, this.httpOptions);
  }
  updateData(id: number, updatedData: any): Observable<any> {
    const url = `${this.baseUrl}/objects/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  deleteData(id: number): Observable<any> {
    const url = `${this.baseUrl}/objects/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  uploadFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);

    return this.http.post<any>(`${this.baseUrl}/objects/loadfile/`, formData, this.httpFile);
  }


  // Реестр договоров => вывод / обновление / добавление
  getWorkReg(): Observable<any> {
    return this.http.get(`${this.baseUrl}/contracts/`)
  }
  СontractsList(id: number): Observable<listContracts[]> {
    const url = `${this.baseUrl}/contracts/${id}/`;
    return this.http.get<listContracts[]>(url);
  }
  updСontractsList(id: number, updatedData: any): Observable<listContracts[]> {
    const url = `${this.baseUrl}/contracts/${id}/`;
    return this.http.put<listContracts[]>(url, updatedData);
  }
      // Добавление договора
  addContractsCreate(ContractsCreate: ContractsCreates, ContractsData:ContractsCreates ): Observable<ContractsCreates[]> {
    const url = `${this.baseUrl}/contracts/create/`;
    return this.http.post(url, ContractsCreate, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
    // Получение Код контрагента
  getContractsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/contracts/create/`)
  }
  addContractsList(contract_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/`;
    return this.http.post(url, fact, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
    // Таблица Платежи факт
  getPaymentsFact(facttId: number): Observable<paymentsFact[]> {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/`;
    return this.http.get<paymentsFact[]>(url);
  }
  // Формирование требования на оплату
  addPaymentsFactDocx(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/create_docx/`;
    return this.http.post<factDetails[]>(url, fact); // Просто верните response без изменений
  }

    // Формирование счетов-фактур
  addPaymentsFactInvoices(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactInvoices(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<invoices[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/${itemId}/`;
    return this.http.put<invoices[]>(url, updatedData);
  }
  delPaymentsFactInvoices(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
    // Формирование счетов на предоплату
  addPaymentsFactPrepay(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactPrepay(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<prepay_invoices[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/${itemId}/`;
    return this.http.put<prepay_invoices[]>(url, updatedData);
  }
  delPaymentsFactPrepay(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
    // Формирование пунктов плана
  addPaymentsFactItems(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactItems(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<items[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/${itemId}/`;
    return this.http.put<items[]>(url, updatedData);
  }
  delPaymentsFactItems(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
    // Получение подробной инофрмации в таблице Платежи факт
  getPaymentsFactDetails(facttId: number, paymentId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/`;
    return this.http.get<factDetails[]>(url);
  }
  editPaymentsFactDetails(contract_id: number, payment_id: number, updatedData: any): Observable<items[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/`;
    return this.http.put<items[]>(url, updatedData);
  }
      // Таблица Платежи план
  getPaymentsList(contractId: number): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/`;
    return this.http.get<payments[]>(url);
  }
  addPaymentsList(contractId: number, payment: payments): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/`;
    return this.http.post(url, payment, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsList(contractId: number, paymentId: number, updatedData: any): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/${paymentId}/`;
    return this.http.put<payments[]>(url, updatedData);
  }
  delPaymentsList(contractId: number, paymentId: number): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments/${paymentId}/`;
    return this.http.delete<payments[]>(url);
  }
    // Таблица Акты
  getActsList(actsId: number): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${actsId}/acts/`;
    return this.http.get<acts[]>(url);
  }
  addActsList(contractId: number, acts: acts): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/`;
    return this.http.post(url, acts, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  editActsList(contractId: number,actsId: number, updatedData: any): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/${actsId}/`;
    return this.http.put<acts[]>(url, updatedData);
  }
  delActsList(contractId: number, actsId: number): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/${actsId}/`;
    return this.http.delete<acts[]>(url);
  }


  // Объекты аналоги => добавление
  getAnalogBudget(): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/`)
  }
  getAnalogId(AnalogId: number): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/${AnalogId}`;
    return this.http.get<analogBudgets[]>(url);
  }
  addAnalogBudget(analogBudget: analogBudgets): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addWorkPackage(WorkPackage: WorkPackages): Observable<WorkPackages[]> {
    const url = `${this.baseUrl}/work_package/`;
    return this.http.post(url, WorkPackage).pipe(
      map((response: any) => response.body)
    );
  }
  addEquipment(Equipment: Equipments): Observable<Equipments[]> {
    const url = `${this.baseUrl}/analog_budget_equipment/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  getСomparison(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/comparison/`)
  }


  // Оценочный бюджет => вывод / добавление
  getEstimated(): Observable<Estimated[]> {
    return this.http.get<Estimated[]>(`${this.baseUrl}/estimated_budget/`)
  }
  getEstimatedId(EstimatedId: number): Observable<Estimated[]> {
    const url = `${this.baseUrl}/estimated_budget/${EstimatedId}`;
    return this.http.get<Estimated[]>(url);
  }
  getworkPackage(): Observable<workPackage[]> {
    const url = `${this.baseUrl}/work_package/`;
    return this.http.get<workPackage[]>(url);
  }
  getEquipment(): Observable<Equipments[]> {
    const url = `${this.baseUrl}/analog_budget_equipment/`;
    return this.http.get<Equipments[]>(url);
  }
  getwAnalog(analogId: number): Observable<analog[]> {
    const url = `${this.baseUrl}/analog_budget/${analogId}/`;
    return this.http.get<analog[]>(url);
  }
  addAnalog(WorkPackages: workPackage): Observable<analog[]> {
    const url = `${this.baseUrl}/work_package/`;
    return this.http.post(url, WorkPackages ).pipe(
      map((response: any) => response.body)
    );
  }
  addBudget(WorkPackages: workPackage): Observable<analog[]> {
    const url = `${this.baseUrl}/analog_budget/`;
    return this.http.post(url, WorkPackages ).pipe(
      map((response: any) => response.body)
    );
  }
  addEstimated(Estimated: Estimated): Observable<Estimated[]> {
    const url = `${this.baseUrl}/budget_block/`;
    return this.http.post(url, Estimated ).pipe(
      map((response: any) => response.body)
    );
  }


  // СДР => добавление / вывод / обновление / удаление
  getWbs(): Observable<Wbs[]> {
    return this.http.get<Wbs[]>(`${this.baseUrl}/wbs/get_node/`)
  }
  addProjectFilter(ProjectFilter: ProjectFilter): Observable<ProjectFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addDirectionFilter(DirectionFilter: DirectionFilter): Observable<DirectionFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addObjectFilter(ObjectFilter: ObjectFilter): Observable<ObjectFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addSystemFilter(SystemFilter: SystemFilter): Observable<SystemFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addWorkFilter(WorkFilter: WorkFilter): Observable<WorkFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addEstimateFilter(EstimateFilter: EstimateFilter): Observable<EstimateFilter[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }
  deleteProjectPackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/1/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteDirectionPackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/2/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteObjectPackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/3/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteSystemPackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/4/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteWorkPackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/5/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteEstimatePackage(RemovePackage: DeleteEntity, id?: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_package/6/${id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  changeProjectElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/1/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  changeDirectionElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/2/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  changeObjectElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/3/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  changeSystemElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/4/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  changeWorkElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/5/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  changeEstimateElement(ChangeElement: ChangeElement, id: number) {
    const url = `${this.baseUrl}/wbs/update_wbs_element/6/${id}`;
    return this.http.post(url, ChangeElement).pipe(
      map((response: any) => response));
  }
  getProjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/1/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }

  getDirectionElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/2/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }

  getObjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/3/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }

  getSystemElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/4/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }

  getWorkElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/5/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }

  getEstimateElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/6/`;
    return this.http.post(url, ElementFilter).pipe(
    map((response: any) => response));
  }

  addProjectElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/1/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }
  addDirectionElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/2/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }
  addObjectElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/3/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }
  addSystemElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/4/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }
  addWorkElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/5/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }
  addEstimateElement(AddedElement: CreateElement) {
    const url = `${this.baseUrl}/wbs/create_wbs_element/6/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }

  deleteProjectElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/1/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }
  deleteDirectionElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/2/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }
  deleteObjectElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/3/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }
  deleteSystemElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/4/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }
  deleteWorkElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/5/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }
  deleteEstimateElement(RemoveElement: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_element/6/`;
    return this.http.post(url, RemoveElement).pipe(
      map((response: any) => response));
  }


  // Макропараметры => добавление / вывод
  getParameter(): Observable<CurrencyExchange[]> {
    return this.http.get<CurrencyExchange[]>(`${this.baseUrl}/macroparameters/parameter_value/`)
  }
  getWacc(): Observable<Wacc[]> {
    return this.http.get<Wacc[]>(`${this.baseUrl}/macroparameters/discount_rates/`)
  }
  getSelect(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/macroparameters/parameters/`)
  }
  addDiscountRate(DiscountRate: DiscountRate): Observable<DiscountRate[]> {
    const url = `${this.baseUrl}/macroparameters/create_discount_rate/`;
    return this.http.post(url, DiscountRate).pipe(
      map((response: any) => response.body)
    );
  }
  addParameters(Parameters: Parameters): Observable<Parameters[]> {
    const url = `${this.baseUrl}/macroparameters/create_parameter/`;
    return this.http.post(url, Parameters ).pipe(
      map((response: any) => response.body)
    );
  }
  addParameterValue(ParameterValue: ParameterValue): Observable<ParameterValue[]> {
    const url = `${this.baseUrl}/macroparameters/create_parameter_value/`;
    return this.http.post(url, ParameterValue ).pipe(
      map((response: any) => response.body)
    );
  }
}
