import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule, NgFor } from '@angular/common';
// import { NgxLocalStorageModule } from 'ngx-localstorage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from "@angular/material/form-field";

//===== Main Page =======
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './authentication/login/login/login.component';
import { SignupComponent } from './authentication/login/signup/signup.component';
import { RestoreComponent } from './authentication/login/restore/restore.component';
import { ProfileComponent } from './authentication/login/profile/profile.component';
import { NotFoundComponent } from 'src/404/not-found/not-found.component';

//======== Content =========
import { HomeComponent } from './content/home/home.component';
import { HeaderComponent } from './header/header.component';
import { CardObjComponent } from './content/budget/card-obj/card-obj.component';
import { RegisterContractsComponent } from './content/contract/register-contracts/register-contracts.component';
import { AnalogyComponent } from './content/references/analogy/analogy.component';
import { WbsComponent } from './content/budget/wbs/wbs.component';
import { MacroComponent } from './content/references/macro/macro.component';
import { EstimatedComponent } from './content/budget/estimated/estimated.component';

//===== Service Page =======
import { ThemeService } from './theme.service'
import { AuthenticationService } from './authentication/login/login/authentication.service'
import { AuthGuard } from './authentication/login/helpers/auht.guard';
import { AuthInterceptor } from './authentication/login/helpers/auth.interceptor';

//======== Plfgins =========
// import { GoogleChartsModule } from 'angular-google-charts';
import { NgxMaskModule } from 'ngx-mask'

//==== Material Design ====
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { ModaDialogComponent } from './content/home/moda-dialog/moda-dialog.component';
import { MatRadioModule } from '@angular/material/radio';
import {MatBadgeModule} from '@angular/material/badge';
import { UsersComponent } from './content/users/users.component';

const appRoutes: Routes = [
  //======== Auth page =========
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'restore', component: RestoreComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },

  //======== Content =========
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'card-obj', component: CardObjComponent, canActivate: [AuthGuard] },
  { path: 'register-contracts', component: RegisterContractsComponent, canActivate: [AuthGuard] },
  { path: 'analogy', component: AnalogyComponent },
  { path: 'wbs', component: WbsComponent },
  { path: 'macro', component: MacroComponent },
  { path: 'estimated', component: EstimatedComponent },
  { path: 'users', component: UsersComponent },

  //======== Page 404 =========
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent, SignupComponent, MenuComponent, RestoreComponent, ProfileComponent, HeaderComponent,ModaDialogComponent,
    MacroComponent, EstimatedComponent, HomeComponent, CardObjComponent, RegisterContractsComponent, AnalogyComponent, WbsComponent
  ],
  imports: [
    NgxMaskModule,
    MatFormFieldModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    CommonModule,
    RouterModule,
    MatInputModule, MatExpansionModule, MatIconModule, 
    MatSidenavModule, MatSelectModule, MatButtonModule,
     MatSlideToggleModule, MatStepperModule, MatCardModule, MatRadioModule, MatBadgeModule,
     MatDividerModule, MatTooltipModule, MatMenuModule, 
     MatPaginatorModule, MatTableModule, MatDatepickerModule, 
     MatNativeDateModule, MatTabsModule, MatCheckboxModule, MatListModule, 
     MatTreeModule, MatButtonToggleModule, NgFor, MatTableModule, MatButtonModule, 
     MatDialogModule, MatDialogModule, MatCardModule,// NgxLocalStorageModule.forRoot()
  ],
  providers: [
    ThemeService,
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
